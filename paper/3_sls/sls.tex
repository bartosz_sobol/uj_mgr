%%
%% Author: Bartosz Sobol
%% 22.04.2019
%%

\chapter{PEG w programie SLS}\label{ch:sls}
\paragraph*{}
Znajdujący się obecnie w ostatniej fazie realizacji program konstrukcji rakiety nośnej Space Launch System (SLS)
ma zastąpić zakończony w 2011 roku program amerykańskich promów kosmicznych.
Po tym jak dzięki rakiecie Saturn V oraz kapsule Apollo ludzie postawili stopę na Księżycu,
a następnie wahadłowce STS umożliwiły zbudowanie Międzynarodowej Stacji Kosmicznej,
która utrwaliła obecności człowieka w przestrzeni kosmicznej,
rakiety SLS mają być kolejną konstrukcją realizująca najważniejsze misje eksploracyjne - powrót ludzi na Księżyc (SLS Block-1 i Block-1B),
a docelowo załogowy lot na Marsa (SLS Block-2).

Nowy projekt dzieli wiele rozwiązań technologicznych ze swoim poprzednikiem - wahadłowcem STS\@.
Ponownie użyte będą miedzy innymi silniki główne RS-25, rakiety pomocnicze na paliwo SRB stałe czy konstrukcja zbiorników paliwa.
W przypadku ponownego użycia tak wielu rozwiązań, naturalne wydawałoby się także zastosowanie tego samego algorytmu sterowania.
Z drugiej jednak strony natura misji realizowanych przez SLS będzie wymagała dłuższych lotów na bardziej energetyczne trajektorie,
a wtedy wynikające w założeń poczynionych w procesie projektowania ograniczenia algorytmu PEG mogą okazać się zbyt duże.


\section{Wybór algorytmu}\label{sec:wybór-algorytmu}
\paragraph*{}
Algorytm zastosowany w projekcie SLS musi być zdolny do skutecznego i bezpiecznego sterowania statkiem kosmicznym podczas wszystkich, bardzo różnorodnych misji planowanych dla nowej rakiety.
Podczas symulacji z zastosowaniem niezmodyfikowanego algorytmu PEG zaobserwowano trudności przy osiąganiu trudniejszych trajektorii~\cite{SLS-STUDY}.
Próby te pokazały potrzebę analizy możliwości i opłacalności zastosowania metody, na której opiera się algorytm PEG oraz porównania go z alternatywnym rozwiązaniem.


\subsection{Algorytm OPGUID}\label{subsec:algorytm-opguid}
\paragraph*{}
Algorytm PEG został poddany analizie porównawczej~\cite{SLS-STUDY} z algorytmem OPGUID, który od lat 60-tych rozwijano równolegle z PEG,
jednak finalnie nigdy nie został użyty w trakcie misji.
W przeciwieństwie do PEG, który przybliża sterowanie optymalne opierając się na rozwiązaniu czasooptymalnym, skuteczność OPGUID została zapewniona poprzez ścisłe
spełnienie warunków sterowania optymalnego łącznie z równaniami Eulera-Lagrange'a~\cite{SLS-STUDY}.


\subsection{Metody porównawcze}\label{subsec:metody-porownawcze}
\paragraph*{}
Jako misję referencyjną przyjęto lot rakiety SLS Block-1B na niską orbitę okołoziemską z kontynuacją w formie manewru TLI (Trans Lunar Injection).
Zachowanie algorytmów badano w warunkach nominalnych oraz w wypadku awarii jednego z silników.
Do symulacji działania obu algorytmów zastosowano metodę Monte Carlo.\\
Algorytmy zostały ocenione pod kątem~\cite{SLS-STUDY}:

\paragraph{Ryzyka utraty misji:} Podstawowe kryterium określające czy wskaźnik utraty misji - \textit{failure rate} - jest akceptowalny.
\paragraph{Ryzyka projektowego:} Nietechniczne ryzyka związane z zastosowaniem danego algorytmu oraz czynniki takie jak dostępność dokumentacji oraz historia zastosowania.
\paragraph{Założeń i ograniczeń:} Liczba założeń i ograniczeń oraz ich wpływ na możliwości zastosowania algorytmu.
\paragraph{Elastyczności:} Możliwość zastosowania algorytmu w szerokim spektrum misji planowanych dla SLS oraz takich, które nie są jeszcze planowane, ale technicznie wykonalne, a także łatwość modyfikacji algorytmu.
\paragraph{Złożoności:} Złożoność obliczeniowa oraz skomplikowanie kodu.
\paragraph{Danych wejściowych:} Liczba parametrów wejściowych.
\paragraph{Stabilności:} Zdolność od osiągania zbieżności, szybkość osiągania zbieżności oraz odporność na rozrzut.
\paragraph{Skuteczności:} Dokładność w osiąganiu zamierzonej trajektorii oraz optymalność mierzona ilością zużytego paliwa.


\section{Wyniki i wnioski}\label{sec:wyniki-i-wnioski}
\paragraph{Ryzyko utraty misji} Oba algorytmy uzyskały satysfakcjonujące wyniki zarówno w scenariuszu nominalnym jak i z awarią silnika.
\paragraph{Ryzyko projektowego} Dzięki historii wykorzystania PEG w programie STS oraz trwające już prace nad wprowadzeniem
nowoczesnych standardów do kodu źródłowego algorytmu stwierdzono zdecydowanie mniejsze ryzyka projektowe niż w przypadku wykorzystania metody OPGUID.
\paragraph{Założenia i ograniczenia} Z uwagi na założenie jednorodnego pola grawitacyjnego oraz przybliżenia poczynione w~(\ref{sec:blok-5})
metoda PEG wykazała spadek dokładności przy dłuższych manewrach takich jak Trans Lunar Injection.
Wskazano potrzebę wprowadzenia modyfikacji w celu zagwarantowania skuteczności sterowania w trakcie tego typu misji.
\paragraph{Elastyczność} Zarówno PEG jak i OPGUIOD posiadają konstrukcję modularną, wykazują duże możliwości rozbudowy, a ich poszczególne sekcje,
takie jak model grawitacyjny czy metoda ekstrapolacji stanu, mogą być modyfikowane niezależnie od pozostałych.
\paragraph{Złożoność} Algorytm PEG okazał się wykonywać obliczenia potrzebne do uzyskania komend sterowania o rząd wielkości szybciej.
Dodatkowo kod programu był o 40\% krótszy.
\paragraph{Dane wejściowe} Obie metody wymagają podobnych danych wejściowych i liczby parametrów.
\paragraph{Stabilność} Algorytm PEG wymagał średnio mniejszej liczby iteracji do uzyskania zbieżności zarówno w przypadku nominalnym jak i z awarią silnika.
Różnice nie były jednak znaczące.
\paragraph{Skuteczność i dokładność} Analizowane metody wykazały zbliżoną, wysoką skuteczność i dokładność w osiąganiu zakładanej trajektorii.

\paragraph*{}
Końcowe wyniki analizy okazały sie zbliżone dla obu algorytmów:
\begin{table}
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        \multirow{2}{*}{\bfseries{Kategoria}} & \multirow{2}{*}{\bfseries{Waga}} & \multicolumn{2}{c|}{\bfseries{Wynik}} \\
        \cline{3-4}
        & \ & PEG & OPGUID \\
        \hline
        Ryzyko utraty misji & 0/1 & 1 & 1\\
        Ryzyko projektowe & 8\% & 5.2 & 2.8\\
        Założenia i ograniczenia & 3\% & 0.4 & 2.6\\
        Elastyczność & 20\% & 10 & 10\\
        Złożoność & 16\% & 9.6 & 6.4\\
        Dane wejściowe & 4\% & 2 & 2\\
        Stabilność & 21\% & 10.5 & 10.5\\
        Skuteczność i dokładność & 28\% & 14 & 14\\
        \hline
    \end{tabular}
    \caption{Porównanie PEG i OPGUID~\cite{SLS-STUDY}}
\end{table}

W pięciu kategoriach metody PEG i OPGUID otrzymały taką samą punktację, w dwóch przewagę uzyskał PEG,
a OPGUID okazał sie wyraźnie lepszy tylko pod względem założeń i ograniczeń.
Finalnie do użycia w rakietach SLS Block-1 i Block-1B został wybrany algorytm PEG\@.

W trakcie testów w algorytmie PEG poczyniono szereg modyfikacji,
które wpłynęły na poprawę zbieżności oraz rozszerzyły zakres osiągalnych trajektorii.
Zmiany te w praktyce zniwelowały negatywny wpływ założeń i przybliżeń stosowanych podczas formułowania sterowania
oraz zostały na stałe wcielone do kodu PEG przeznaczonego do użycia w systemach rakiet SLS\@.


\section{Modyfikacje}\label{sec:modyfikacje}
\paragraph*{}
W trakcie projektowania systemu sterowania lotem dla rakiety SLS Block-1B poczyniono szereg zmian w stosunku do systemu stosowanego w promach STS\@.
Większość z tych adaptacji dotyczy podsystemów zarządzających i kontrolujących działanie algorytmu PEG, jak na przykład procedura zarządzająca fazami lotu,
i wynikają one z różnic konstrukcyjnych między STS, a SLS\@.
Sama metoda PEG została w większości niezmieniona. Wprowadzono jednak modyfikacje mające zapewnić poprawne działanie
algorytmu podczas szczególnie wymagających misji planowanych dla SLS\@.


\subsection{Turn rate vector}\label{subsec:turn-rate-vector}
\paragraph*{}
Założenie z Bloku 5~(\ref{sec:blok-5}) o ortogonalności $\vec{\lambda}$ i $\vec{\lambda '}$ pozostaje bliskie optymalnemu w większości standardowych przypadków.
Jednakże w sytuacji skrajnie niskiego stosunku ciągu do masy, występującej głownie w wypadku awarii silnika, kierunek ciągu zbliża się do kierunku radialnego co powoduje spadek optymalności sterowania.

\subsubsection{Ograniczenie kąta ciągu}
\paragraph*{}
Jako rozwiązanie powyższego problemu zaproponowano ograniczenie kąta ciągu $\phi$ w następujący sposób
\[\tan(\phi)=(J/L)\vec{\lambda '},\]
co daje ograniczenie na $\vec{\lambda '}$
\[\vec{\lambda '_{\phi}}=\tan(\phi)(L/J),\]
gdzie $J$ oraz $L$ to wartości odpowiednich całek z Bloku 4~(\ref{sec:blok-4}), a $J/L$ to w przybliżeniu połowa pozostałego czasu manewru.

\subsubsection{Zabezpieczenie przed ciągiem wstecznym}
\paragraph*{}
W przypadku kiedy $\vec{\lambda '}$ jest bliski wektorowi radialnemu lub przy wyjątkowo długich okresach niskiego ciągu istnieje ryzyko przybrania przez wersor ciągu kierunku wstecznego.
Wtedy konieczne jest dalsze ograniczenie $\vec{\lambda '}$.
Podobnie jak poprzednio stosujemy
\[\tan(\theta+\delta)=(J/L)\vec{\lambda '},\]
co daje nam ograniczenie na $\vec{\lambda '}$
\[\vec{\lambda'}_{EL}=\tan(\theta+\delta)(L/J),\]
\begin{figure}[h]
    \caption{Ograniczenia na $\vec{\lambda'}$~\cite{SLS-PEG}}
    \centering
    \includegraphics[width=0.8\textwidth]{./img/sls_ltg.png}
    \label{fig:sls-ltg}
\end{figure}\\
gdzie $(x,z)$ $(x_c,z_c)$ oznaczają kierunki radialny i horyzontalny odpowiednio w chwili wykonywania obliczeń
oraz w położeniu docelowym, $\theta$ jest kątem między $\vec{\lambda}$, a kierunkiem radialnym w położeniu docelowym, a $\delta>0$ jest pewnym kątem ograniczającym.

\subsection{Predykcja $\vec{v}_{go}$}\label{subsec:zbieznosc-algorytmu}
\paragraph*{}
Algorytm PEG stosuje schemat predykcyjno-korekcyjny do wyznaczania w kolejnych iteracjach wektora $\vec{v}_{go}$ tak, aby różnica pomiędzy prędkością $\vec{v}_{p}$ w położeniu $\vec{r}_{p}$ zbiegała do zera.
Konsolidując obliczenia z Bloków: 5~(\ref{sec:blok-5}) i 8~(\ref{sec:blok-8}) predykcję i korekcję dla $\vec{v}_{go}$ możemy przedstawić następującymi równaniami
\begin{align*}
    \vec{v}_{miss}&=\vec{v}_{p}-\vec{v}_{d},\\
    \Delta\vec{v}_{go}&=\vec{v}_{miss},\\
    \vec{v}_{go,n}&=\vec{v}_{go,n-1}-\Delta\vec{v}_{go},
\end{align*}
co implikuje założenie, że korekcja $\vec{v}_{go}$ jest równa zmianie $\vec{v}_{p}$.
Jakobian korekcji ma postać
\[\mathbf{J}=\frac{\delta\vec{v}_{miss}}{\delta\vec{v}_{go}}\approx\mathbf{I}.\]
W przypadku manewrów z długimi czasami pracy silników założenie to prowadzi do niestabilnych korekcji i oscylujących wartości $\vec{v}_{go}$.
Aby przeciwdziałać temu zjawisku empirycznie wyznaczono współczynnik zmniejszający korekcje równy $0.5$.
Wtedy
\[\mathbf{J}=0.5\mathbf{I}\]
oraz
\[\Delta\vec{v}_{go}=0.5\vec{v}_{miss}.\]


\subsection{Orbity hiperboliczne}\label{subsec:niestandardowe-trajektorie}
\paragraph*{}
Trajektorią docelową dla misji zakładających opuszczenie ziemskiego pola grawitacyjnego jest orbita otwarta hiperboliczna.
Aby umożliwić stosowanie metody PEG w lotach na trajektorie hiperboliczne dodano nowy tryb pracy algorytmu zmieniający sposób obliczania prędkości $\vec{v}_{d}$ opisany w Bloku 8~(\ref{sec:blok-8}).\\

Niech $\vec{v}_{e}$ oznacza \textit{hyperbolic excess velocity} czyli różnicę prędkości obiektu poruszającego się po trajektorii hiperbolicznej oraz prędkości ucieczki.
Jeśli dane jest $\vec{v}_{e}$ dla pewnej orbity docelowej, to prędkość $\vec{v}_{d}$ obliczamy następująco:
\begin{align*}
    &\hat{v}_{e}=\operatorname{unit}(\vec{v}_{e}),\ \hat{r}_{p}=\operatorname{unit}(\vec{r}_{p}),\\
    \vec{v}_{t}&=\frac{1}{2}|\vec{v}_{e}|\left((D+1)\hat{v}_{e}+(D-1)\hat{r}_p\right),
\end{align*}
gdzie
\[D=\sqrt{1+\frac{4\mu_{e}}{|\vec{r}_{p}||\vec{v}_{e}|^{2}(1+\hat{r}_{p}\circ\hat{v}_{e})}}.\]
Wtedy
\[\vec{v}_{d}=\vec{v}_{t}-(\vec{v}_{t}\circ\vec{\imath}_{y})\vec{\imath}_{y}.\]

