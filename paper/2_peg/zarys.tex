%%
%% Author: Bartosz Sobol
%% 22.04.2019
%%

\paragraph*{}
Zadaniem algorytmu sterowania lotem jest doprowadzenie sterowanego statku od stanu (prędkość, położenie) początkowego do ustalonej trajektorii (orbity) docelowej.
Innymi słowy potrzebne jest takie dobieranie kierunku wektora ciągu silników, aby w sposób jak najbliższy optymalnemu osiągnąć pewien pożądany stan i w nim zakończyć pracę.

Jednym w możliwych rozwiązań tego problemu jest prezentowany algorytm Powered Explicit Guidance.
Jego korzenie sięgają lat 60-tych i stosowanego w rakiecie Saturn V Iterative Guidance Mode (IGM).
Zarówno PEG jak i IGM opierają się na metodzie Linear Tangent Guidance (LTG) zakładającej,
że tangensy kątów pitch i yaw są funkcjami postaci $\frac{f(t)}{g(t)}$ gdzie $f$ i $g$ są liniowymi funkcjami czasu.\\

Metoda PEG~\cite{UPFG} stosuje iteracyjny schemat predykcyjno-korekcyjny, tzn.
w każdej iteracji algorytmu przewidywany jest stan końcowy sterowanego statku.
Komendy sterowania formułowane są tak, aby przewidywany stan końcowy zbiegał do stanu docelowego.

Sama procedura PEG ma formę układu za sprzężeniem zwrotnym.
Jest ona wywoływana cyklicznie przez zewnętrzny program przez cały czas trwania manewru, gdzie przez manewr będziemy rozumieć część lotu od pierwszego wywołania algorytmu
do osiągnięcia zamierzonej trajektorii.
Główna pętla może zostać wywołana w dwóch trybach: aktywnym i pasywnym.
Przed przejęciem kontroli nad sterowaniem algorytm działa w trybie pracy pasywnej.
Wywołanie pasywne ma na celu inicjalizację zmiennych potrzebnych do przejścia w tryb aktywny - rozpoczęcia faktycznego sterowania - oraz
doprowadzenia do zbieżności algorytmu tj.
sytuacji kiedy dwa kolejne przewidywania $\Delta v$ potrzebnej do osiągnięcia planowanej trajektorii nie różnią sie w sposób znaczący
(precyzyjnych wartości opisano w~\cite{UPFG} i zostały one wyznaczone empirycznie w implementacji~\ref{sec:lot-egz-peg}).

Komendy sterowania wydawane sa na podstawie jawnych, przybliżonych rozwiązań równań ruchu obliczanych w trakcie manewru w każdym przebiegu procedury i mogą one dotyczyć:
\begin{itemize}
    \item zmiany kierunku wektora ciągu silników,
    \item przejścia do następnej fazy manewru (najczęściej uruchomienia kolejnego stopnia rakiety),
    \item zmiany siły ciągu silników.
\end{itemize}
PEG przewiduje dwa modele sterowania mocą silników: utrzymywania stałej siły ciągu oraz utrzymywania stałego przyspieszenia.
W każdej fazie manewru może być używany inny tryb pracy.

\section{Założenia}\label{sec:peg-zalozenia}
\paragraph*{}
Podczas formułowania samego algorytmu, którego szczegóły działania zostaną omówione w następnym rozdziale, przyjęto następujące założenia:
\begin{enumerate}
    \item W momencie startu algorytmu statek znajduje się w ruchu,
    \item Jednorodne pole grawitacyjne,
    \item Opór powietrza jest pomijalny (lot egzoatmosfetyczny),
    \item Pomijalny wpływ grawitacyjny innych ciał (Księżyca).
\end{enumerate}

\section{Układy odniesienia i stan końcowy}\label{sec:uklady-odniesienia}
\paragraph*{}
W trakcie obliczeń będziemy poruszać się w dwóch układach odniesienia.
\subsection{Inercjalny układ odniesienia}\label{subsec:inercjalny-uklad-odniesienia}
Układ odniesienia określony przez prostokątny układ współrzędnych z początkiem w środku ciężkości Ziemi i wyznaczonym przez wektory
\[\vec{q}_x,\ \vec{q}_y,\ \vec{q}_z,\]
gdzie $\vec{q}_y$ zwrócony jest w kierunku bieguna północnego, a $\vec{q}_x$ oraz $\vec{q}_z$ to dwa dowolnie wybrane, prostopadłe kierunki w płaszczyźnie równika.
W układzie tym będziemy wyznaczać prędkość oraz położenie statku.

\subsection{Orbitalny układ odniesienia}\label{subsec:orbitalny-uklad-odniesienia2}
Drugi układ odniesienia, używany do opisu trajektorii, określamy przez prostokątny układ współrzędnych z początkiem w środku ciężkości statku i wyznaczonym przez wektory
\[\vec{\imath}_x,\ \vec{\imath}_y,\ \vec{\imath}_z,\]
które są odpowiednio: wektorem radialnym, wektorem normalnym do trajektorii oraz wektorem w kierunku ruchu zaczepionymi w położeniu docelowym.

\section{Stan końcowy}\label{sec:stan-koncowy}
\paragraph*{}
Pożądany stan końcowy będziemy określać poprzez:
\begin{itemize}
    \item wysokość (długość promienia),
    \item prędkość,
    \item płaszczyznę trajektorii określoną przez inklinację orbity oraz długość węzła wstępującego,
    \item kąt ścieżki lotu $\gamma=\arccos\left(\frac{\vec{r}\circ\vec{v}}{\vec{r}||\vec{v}|}\right)$ gdzie $\vec{r}$ i $\vec{v}$ to położenie i prędkość statku.
\end{itemize}
Daje to łącznie 5 parametrów równoważnych z klasycznym elementami orbitalnymi.
Parametry te pozwalają na określenie orbity docelowej z dokładnością do argumentu perycentrum,
którego wartość będzie zależeć od tego jak szybko statek spełni warunki stanu docelowego.
Można więc sterować jego wartością nakładając ograniczenie na maksymalną siłę ciągu oraz ustalając odpowiedni czas startu.

 \section{Opis zmiennych}\label{sec:zmienne}
\begin{itemize}
    \item $n$ - liczba faz manewru,
    \item $k$ - numer bieżącej fazy manewru,
    \item $TM_i$ - sposób sterowania ciągiem w $i$-tej fazie manewru. $const\_acc$ lub $const\_thrust$,
    \item $f_{T,i}$ - zakładana wartość siły ciągu dla $i$-tej fazy manewru,
    \item $a_{T,i}$ - zakładana wartość przyspieszenia wynikającego z ciągu silników dla $i$-tej fazy manewru,
    \item $a_{max,i}$ - górne ograniczenie przyspieszenia dla $i$-tej fazy manewru,
    \item $\vec{r}$ - aktualne położenie statku,
    \item $\vec{r}_d$ - docelowe położenie statku,
    \item $\vec{r}_p$ - przewidywane położenie statku po zakończeniu manewru,
    \item $\vec{r}_{go}$ - przemieszczenie potrzebne do uzyskania położenia docelowego,
    \item $\vec{r}_{go,xy}$ - rzut wektora $\vec{r}_{go}$ na płaszczyznę wyznaczoną przez $\vec{\imath}_x$ oraz $\vec{\imath}_y$,
    \item $\vec{r}_{grav}$ - przemieszczenie statku pod wpływem grawitacji,
    \item $\vec{r}_{thrust}$ - przemieszczenie statku pod wpływem ciągu,
    \item $r_d$  - docelowa wartość promienia,
    \item $r_{go,z}$ - długość rzutu prostokątnego wektora na wektor $\vec{\imath}_z$,
    \item $\Delta t$ - czas wykonywania algorytmu podczas ostatniego wywołania,
    \item $t_{-1}$ - czas startu algorytmu podczas poprzedniego wywołania,
    \item $t_{b,i}$ - pozostały czas pracy silników podczas $i$-tej fazy manewru,
    \item $t_{go,i}$ - czas pozostały do zakończenia $i$-tej fazy manewru, $t_{go}=t_{go,n}$,
    \item $t_{go,prev},\ \vec{r}_{grav, prev},\ \dots$ - wartości z poprzedniej iteracji algorytmu,
    \item $\tau_{i}$ - stosunek masy do wartości przepływu masy dla $i$-tej fazu manewru,
    \item $\dot{m}_i$ - zakładana wartość strumienia masy dla $i$-tej fazy manewru,
    \item $m$ - całkowita masa statku,
    \item $m_{0, i}$ - początkowa całkowita masa statku dla $i$-tej fazy manewru,
    \item $v_{ex,i}$ - zakładana prędkość prędkości gazów wylotowych dla $i$-tej fazu manewru,
    \item $v_d$ - docelowa wartość prędkości,
    \item $\vec{v}_{go}$ - różnica miedzy aktualną prędkością statku, a prędkością do osiągnięcia
    \item $\vec{v}_d$ - -docelowa prędkość statku,
    \item $\vec{v}_{grav}$ - zmiana prędkości statku pod wpływem grawitacji,
    \item $\vec{v}_{thrust}$ - zmiana prędkości statku pod wpływem ciągu,
    \item $\gamma_d$ - docelowy kąt ścieżki lotu,
    \item $\phi$ - kąt pomiędzy $\vec{v}_{go}$, a $u_T$,
    \item $\vec{\lambda}$ - wersor w kierunku $\vec{v}_{go}$,
    \item $u_T$ - wersor ciągu,
    \item $\vec{\imath}_x,\ \vec{\imath}_y,\ \vec{\imath}_z$ - wersory układu współrzędnych.
\end{itemize}

\ \\Przejdźmy teraz do szczegółowej analizy kolejnych bloków.