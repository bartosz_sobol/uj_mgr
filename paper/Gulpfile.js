'use strict';

const gulp = require('gulp');
const exec = require('child_process').exec;

gulp.task('compile_tex', function (cb) {
  exec('pdflatex -interaction nonstopmode -halt-on-error -file-line-error MAIN.tex', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('serve', function () {
    gulp.watch('./**/*.tex', gulp.series('compile_tex'));
});
