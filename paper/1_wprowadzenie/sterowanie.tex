%%
%% Author: Bartosz Sobol
%% 24.03.2019
%%

\section{Sterowanie}\label{sec:sterowanie}
\paragraph*{}
Problem kierowania lotem można sformułować w języku teorii sterowania optymalnego.
Celem będzie wyznaczanie wektora ciągu tak, aby spełnione zostały warunki końcowe przy jednoczesnej maksymalizacji końcowej
masy (lub minimalizacji czasu pracy silników) kierowanego statku.\\


Problem kierowania oraz optymalizacji trajektorii lotu może byc traktowany jako zagadnienie Mayera.
\begin{defin}[Problem Mayera~\cite{CONTROL}]
    \ \\
    Niech $t\in\mathbb{R},\ x=x(t)\in\mathbb{R}^n$ oraz $u=u(t)\in \mathbb{R}^m$.
    $t$ jest zmienna wolną, $x$ zmienną fazową, a $u$ będziemy nazywać sterowaniem lub zmienną sterującą.
    Dodatkowo dany jest zbiór $A\subset\mathbb{R}^{n+1}$, który nazywamy zbiorem ograniczeń.\\
    Problem: znaleźć sterowanie
    \[ u: [t_0,t_1]\rightarrow\mathbb{R}^m,\ t_0\leq t\leq t_1\]
    takie, że:
    \begin{enumerate}
        \item $u$ spełnia układ układ równań różniczkowych i warunków brzegowych postaci:
        \[ \begin{cases}
               \dot{x} = f(t, x(t), u(t)),\ t_0\leq t\leq t_1,\ f: \mathbb{R}\times\mathbb{R}^n\times\mathbb{R}^m \rightarrow \mathbb{R}^n, f\in\mathcal{C}^1 \\
               c(x) = (t_0,x(t_0), t_1, x(t_1)) \in B \subset\mathbb{R}^{2n+2}
        \end{cases} \]
        gdzie $B$ jest zbiorem akceptowalnych warunków brzegowych,
        \item $\forall t\in[t_0,t_1]: (t,x(t))\in A$,
        \item $u$ minimalizuje zadany funkcjonał kosztu postaci:
        \[J(u)=g(t_0,x(t_0), t_1,x(t_1)): \mathbb{R}\times\mathbb{R}^n\times\mathbb{R}\times\mathbb{R}^n\rightarrow \mathbb{R}.\]
    \end{enumerate}
\end{defin}

\begin{defin}[Sterowanie optymalne]
    \ \\
    Sterowaniem optymalnym będziemy nazywać sterowanie $u^*$ minimalizujące funkcjonał kosztu z zagadnienia Mayera.
\end{defin}

\subsection{Problem sterowania lotem na orbitę}\label{subsec:problem-sterowania-lotem-na-orbitę}
\paragraph*{}
W przypadku omawianego problemu osiągania orbity zmienną sterującą będzie wersor ciągu $u_{T}(t)$, warunkami brzegowymi będą parametry orbity docelowej, a zbiór ograniczeń będzie determinowany przez możliwości rakiety nośnej.
Z kolei funkcjonał kosztu będzie odpowiadał wymogowi jak największej masy końcowej.
Jeśli założymy, że podczas wznoszenia na pierwsza orbitę silniki pracują nieprzerwanie z niewielkimi wahaniami zużycia paliwa,
to wymóg największej masy końcowej jest\\ w przybliżeniu równoważny z najkrótszym czasem lotu.
Poszukiwane sterowanie optymalne dla statku kosmicznego będzie więc sterowaniem czasooptymalnym.

\subsection*{Założenia}
Aby skonkretyzować i uściślić rozważany problem poczynimy następujące założenia:
\begin{enumerate}
    \item W momencie rozpoczęcia sterowania statek znajduje się w ruchu i znana jest jego aktualna prędkość oraz położenie.
    \item Zakładamy pomijalny opór powietrza (lot egzoatmosfetyczny), jedynymi siłami działającymi na statek są ciąg silników oraz ziemska grawitacja.
    \item Orbita docelowa jest poprawnie określona i możliwa do osiągnięcia przez sterowany statek.
    \item Siła ciągu ma stałą wartość.
    \item Przyspieszenie grawitacyjne zmienia się liniowo względem położenia statku zgodnie z Definicją~\ref{defin-grav-lin}
\end{enumerate}
\begin{defin}[Grawitacja liniowa~\cite{OPT_CON_2}]
    \label{defin-grav-lin}
    Przyjmujemy, że
    \begin{equation}
        g(r)=\frac{GM\vec{r}}{r_0^3}=\frac{\mu_e\vec{r}}{r_0^3}=\omega_0^2\vec{r},
    \end{equation}
    gdzie
    \begin{itemize}
        \item $\mu_e=GM$ jest parametrem grawitacyjnym Ziemi,
        \item $r_0$ jest pewnym promieniem referencyjnym.
    \end{itemize}
\end{defin}

\subsection{Problem sterowania}\label{subsec:problem-sterowania}
Równania ruchu w polu grawitacyjnym~\eqref{eq:ruch-graw} wraz z warunkami końcowymi
\begin{equation}
    c_i=c_i(\vec{r_d}, \vec{v_d})=0,\ i=1,\dots,k\ \ \ k\leq6
\end{equation}
odpowiadającymi parametrom określającym orbitę docelową oraz funkcjonałem kosztu postaci
\begin{equation}
    J =-\int_{t_0}^{t_{f}}\dot{m}dt =\int_{t_0}^{t_{f }}\frac{T}{g_{0}I_{sp}}dt
\end{equation}
określają problem sterowania lotem statku kosmicznego na zadaną orbitę.
\subsection{Sterowanie optymalne}\label{subsec:sterowanie-optymalne}
Poszukiwanie sterowania optymalnego rozpoczniemy od zdefiniowania Hamiltonianu
\begin{equation}
    \label{eq:ham}
    H(\lambda_r, \lambda_v, \vec{r}, \vec{v},u_{T},t)=\lambda^{T}f=\lambda^{T}_{r}\vec{v}+\lambda^{T}_{v}\left(\textbf{g} + a_{T}u_{T}\right)+\eta\left(u_{T}^{T}\circ u_{T}-1\right),
\end{equation}
gdzie dwa pierwsze składniki odpowiadają równaniom ruchu dla $\vec{r}$ i $\vec{v}$, trzeci składnik ogranicza wektory sterowania do tych o normie równej 1 oraz:
\begin{itemize}
    \item $a_T, \textbf{g}$ są zdefiniowane tak jak w~(\ref{subsec:ruch-w-polu-grawitacyjnym}),
    \item $\eta$ jest mnożnikiem skalarnym,
    \item $\lambda_r$ i $\lambda_v$ są wektorami kostanu.
\end{itemize}
Po zastosowaniu zasady maksimum~\cite{EVANS},\cite{OPT_CON_2} do Hamiltonianu możemy sformułować problem optymalizacji
\begin{equation}
    H(\lambda_r, \lambda_v, \vec{r}^*, \vec{v}^*, u_{T}^*,t)=\max_{u_{T}}{H(\lambda_r, \lambda_v, \vec{r}^*, \vec{v}^*, u_{T},t)}
\end{equation}
gdzie '*' oznaczają optymalne wartości zmiennych.
Wystarczającym~\cite{OPT_CON_1} warunkiem optymalności jest
\begin{equation}
    \frac{\partial H}{\partial u_{T}}=0,
\end{equation}
Po dokonaniu obliczeń oraz odpowiednich przekształceń otrzymujemy
\begin{flalign*}
    u_{T}^*&=-\frac{T}{2\eta m}\lambda_v,\\
    u_{T}^{T}u_{T}&=\frac{T^2}{4\eta^2m^2}\lambda^T_v\lambda_v=1,\ \|\lambda_v\|=\frac{2\eta m}{T},\\
    \eta&=\frac{-t}{2m}\|\lambda_v\|
\end{flalign*}
co daje optymalny kierunek ciągu
\begin{equation}
    u_{T}^*=\frac{\lambda_v}{\|\lambda_v\|}.
\end{equation}
\subsubsection{Równania kostanu}
Ponownie opierając się na Hamiltonianie~\eqref{eq:ham} zdefiniujmy równanie kostanu
\begin{flalign}
    \lambda'&=
    \begin{cases}
        \lambda'_r=-\frac{\partial H}{\partial\vec{r}}=\lambda_v\\
        \lambda'_v=-\frac{\partial H}{\partial\vec{v}}=-\lambda_r\\
    \end{cases},\\
    \ &\ \nonumber\\
    \ &\ \nonumber\\
    \lambda''_r&=\omega_0^2\lambda'_v=-\omega_0^2\lambda_r,\ \lambda''_r+\omega_0^2\lambda_r=0\nonumber,
\end{flalign}
które posiada rozwiązanie postaci:
\begin{flalign}
    \lambda_r(t)&=\lambda_r(t_0)\cos(\omega_{0}t)+\lambda_v(t_0)\omega_0\sin(\omega_{0}t),\\
    \lambda_v(t)&=-\int_0^t\lambda_{r}dt=-\frac{\lambda_{r0}}{\omega_0}\sin(\omega_{0}t)+\lambda_{v0}\cos(\omega_{0}t) \label{eq:lambdav}
\end{flalign}
gdzie $\lambda_{r0}$ i $\lambda_{v0}$ są nieznanymi warunkami początkowymi dla kostanu.
Można pokazać~\cite{OPT_CON_2}, że rownież równania stanu
\begin{equation}
    \vec{r}(t)=\int_{t_0}^t\vec{v}dt,\ \vec{v}(t)=\int_{t_0}^t(g(t)+a_Tu_{T})dt
\end{equation}
mają analityczne rozwiązanie postaci
\[\begin{bmatrix}
      \vec{r} \\
      \vec{v}
\end{bmatrix}
= \Omega
\begin{bmatrix}
    \vec{r_0} \\
    \vec{v_0}
\end{bmatrix}
+ \Gamma
\begin{bmatrix}
    I_c \\
    I_s
\end{bmatrix},\]
gdzie
\begin{flalign*}
    \Omega &=
    \begin{bmatrix}
        \cos(\omega_{0}t) & \sin(\omega_{0}t)\\
        -\sin(\omega_{0}t) & \cos(\omega_{0}t)
    \end{bmatrix},\
            \Gamma = \frac{1}{\omega_0}
    \begin{bmatrix}
        \sin(\omega_{0}t) & -\cos(\omega_{0}t)\\
        \cos(\omega_{0}t) & \sin(\omega_{0}t)
    \end{bmatrix}
\end{flalign*}
oraz $I_c$, $I_s$ są całkami ciągu
\begin{flalign*}
    I_c(t)&=\int_0^tu_{T}(t)a_T(t)\cos(\omega_{0}t)dt,\\
    I_s(t)&=\int_0^tu_{T}(t)a_T(t)\sin(\omega_{0}t)dt.
\end{flalign*}
To znaczy, że znając rozwiązania równań stanu, kostanu oraz całek ciągu, możemy wyrazić stan końcowy statku jako funkcję $\lambda_{r0}$ i $\lambda_{v0}$.
\subsubsection{Warunek transwarsalności}\ \\
Na koniec sformułujmy warunki transwersalności czyli ograniczenia na stan końcowy oraz Hamiltonian
\begin{flalign*}
    \lambda_f&=\lambda(t_1)=\overline{v}^{T}c_x,\\
    H_f&=H(t_1)=1,
\end{flalign*}
gdzie
\begin{itemize}
    \item $\overline{v}$ jest wektorem stałych mnożników Lagrange'a,
    \item $c_x$ jest macierzą $6\times k$ gradientów warunków końcowych.
\end{itemize}
Pierwotny wektor kostanu może być dowolnie przeskalowany nie zmieniając warunku optymalności.
Przykładowo może on przybrać formę wektora jednostkowego
\begin{equation*}
    \|\lambda_0^T\|=
    \begin{Vmatrix}
        \lambda^T_{v0}\\
        \lambda^T_{r0}
    \end{Vmatrix}
    =1.
\end{equation*}
Aby upewnić się, że końcowy kostan również jest wektorem jednostkowym, dodamy jeszcze jeden prosty warunek
\begin{equation*}
    \|\lambda_f\|=1.
\end{equation*}


\subsubsection{Sterowanie liniowe}\ \\
Jako, że $\lambda'_v = -\lambda_r$, równanie~\eqref{eq:lambdav} możemy zapisać w postaci
\begin{equation}
    \label{eq:lambdav2}
    \lambda_v(t)=\lambda_{v0}\cos(\omega_{0}t)+\frac{\lambda'_{v0}}{\omega_0}\sin(\omega_{0}t)
\end{equation}
dla pewnych $\lambda_{v0}, \lambda_{r0}$.
Następnie przy założeniu, że $\omega_{0}t\approx 0\ (\omega_0\sim 10^{-3}s^{-1})$ równanie~\eqref{eq:lambdav2} może zostać uproszczone do postaci
\begin{equation}
    \lambda_v(t)=\lambda_{v0}+\lambda'_{vo}t.
\end{equation}
Korzystając z faktu, że optymalny wersor ciągu ma postać
\begin{equation}
    u_{T}^*=\frac{\lambda_v}{\|\lambda_v\|}
\end{equation}
możemy przybliżyć sterowanie optymalne lioniowo
\begin{equation}
    u_{T}^*=\frac{At+B}{\|At+B\|},
\end{equation}
gdzie $A\simeq\lambda'_{v0}=-\lambda_{r0}$ oraz $B\simeq\lambda_{v0}$.
\paragraph*{}
Podsumowując, problem sterowania lotem egzoatmosferycznym z funkcją zysku minimalizująca czas trwania manewru można sprowadzić do
rozwiązania problemu
\begin{flalign*}
    &\min_{A,B}t_1\\
    c_i&=c_i(\vec{r_d}, \vec{v_d})=0,\\
    i&=1,\dots,k\ \ \ k\leq6
\end{flalign*}
z 7 niewiadomymi ($\lambda_0$ oraz $t_1$) i $6+1$ ograniczeniami.
