%%
%% Author: Bartosz Sobol
%% 22.04.2019
%%

\chapter{Implementacja}\label{ch:implementacja}
\paragraph*{}
Implementacja algorytmu opisanego w Rozdziale 2 została wykonana w języku Python jako modyfikacja do gry symulacyjnej Kerbal Space Program (KSP),
która posłużyła również jako środowisko testowe.
Dodatkowo wykorzystywana jest biblioteka kRPC~\cite{KRPC} dostarczająca interfejs do komunikacji ze środowiskiem KSP\@.

W Rozdziale 4 umówione zostaną wszystkie aspekty implementacji, które nie były ujęte w specyfikacji algorytmu PEG oraz przebieg programu.
Dodatkowo działanie programu zostanie zaprezentowane na przykładzie lotu na orbitę o typowych parametrach.

\section{Dane wejściowe i parametry orbity docelowej}\label{sec:impl-input-processing}
\paragraph*{}
Jako dane wejściowe program przyjmuje cztery parametry orbity docelowej: apocentrum $ap$, perycentrum $pe$, inklinację $i$ oraz długość węzła wstępującego $\Omega$.
Dane te są następnie tłumaczone na argumenty akceptowane przez algorytm PEG - wartości prędkości $v_d$ oraz promienia $r_d$,
kąt ścieżki lotu $\gamma$ i wersor normalny do trajektorii $\vec{\imath}_y$.\\

Wznoszenie chcemy zakończyć z perycentrum orbity docelowej, a więc długość promienia to po prostu
\[r_d=pe + R,\]
a wartość docelowej prędkości to wartość prędkości w perycentrum:
\[v_d=v_{pe}=\sqrt{2\mu_e\left(\frac{1}{r_d}-\frac{1}{pe+ap}\right)}.\]
Kąt ścieżki lotu w perycentrum orbity jest zawsze równy $0$
\[\gamma= \arccos\left(\frac{r_dv_{pe}}{r_dv_d}\right)=\arccos(1)=0.\]
Na koniec stosując dla wektora jednostkowego prostopadłego do płaszczyzny równika odpowiednie macierze obrotu:
\[R_x=
\begin{bmatrix}
    1 & 0 & 0\\
    0 & \cos(i) & -\sin(i)\\
    0 & \sin(i) & \cos(i)
\end{bmatrix},
R_z=
\begin{bmatrix}
    \cos(\Omega) & -\sin(\Omega) & 0\\
    \sin(\Omega) & \cos(\Omega) & 0\\
    0 & 0 & 1
\end{bmatrix}\]
wyznaczmy wersor normalny do trajektorii czyli płaszczyzny nachylonej do płaszczyzny równika pod katem $i$ oraz obróconej względem osi $z$ (północ-południe) o kąt $\Omega$
\[\vec{\imath}_y= \left(R_{z}R_{x}\right)\circ
\begin{bmatrix}
    0 & 0 & 1
\end{bmatrix}^T.\]

\subsection{Okno startowe}\label{subsec:obliczanie-czasu-startu}
\paragraph*{}
Do osiągnięcia orbity o pewnej ustalonej długości węzła wstępującego $\Omega$ oraz inklinacji $i$ konieczne jest przeprowadzenie startu w momencie,
kiedy platforma startowa jest odpowiednio zorientowana\\ w inercjalnym układzie odniesienia~(\ref{subsec:inercjalny-uklad-odniesienia}).
Czas taki nazywamy oknem startowym.

Aby ustalić optymalny czas startu obliczamy, korzystając z trygonometrii sferycznej,
różnicę między kątem obrotu planety w chwili okna startowego, a kątem obrotu $\alpha$ w chwili wykonywania obliczeń.
Zakładamy przy tym, że start odbędzie się z miejsca o długości geograficznej 0
\[\Delta\alpha=\Omega+\arcsin\left(\frac{\tan(\phi)}{\tan(i)}\right)-\alpha \mod 360.\]
Następnie uwzględniamy długość geograficzną $\lambda$ platformy startowej i uzyskujemy brakujący kąt obrotu do okna startowego
\[\alpha_0=\Delta\alpha-\lambda \mod 360.\]
Mnożąc otrzymany kąt przez czas obrotu planety o $1^{\circ}$ otrzymujemy czas do okna startowego
\[t_0=T_{deg}\alpha_0.\]

\section{Start i lot atmosferyczny}\label{sec:lot-atmosferyczny}
\paragraph*{}
Algorytm PEG przeznaczony jest do prowadzenia lotu poza gęstymi warstwami atmosfery.\\
W przypadku środowiska KSP za odpowiedni na przejęcie kontroli przez metodę egzoatmosferyczną możemy przyjąć pułap 60km.
Poniżej tej wysokości sterowanie lotem musi odbywać się w inny sposób.

\subsection{Azymut startu}\label{subsec:kierunek-lotu}
\paragraph*{}
Aby efektywnie osiągnąć orbitę o zadanej inklinacji już na niskich pułapach należy odginać tor lotu w odpowiednim kierunku.
Przy założeniu, że orbita o inklinacji $i$ jest osiągalna z szerokości geograficznej $\phi$ ($i\geq\phi$),
na której znajduję się platforma startowa, obliczamy~\cite{LAUNCH_HDG} azymut początkowo pomijając ruch obrotowy planety:
\[\beta_{inertial} = \arcsin\left(\frac{\cos(i)}{\cos(\phi)}\right),\]
a następnie, po wzięciu pod uwagę ruchu obrotowego
\begin{align*}
    v_x&=v_d\sin(\beta_{inertial})-v_{rot}(0)\cos(\phi),\\
    v_y&=b_d\cos(\beta_{inertial})
\end{align*}
otrzymujemy finalny azymut startu na orbitę o inklinacji $i$
\[\beta_{rot}=\arctan\left(\frac{v_x}{v_y}\right),\]
gdzie $v_{rot}(\alpha)$ to prędkość liniowa obrotu na powierzchni w szerokości geograficznej $\alpha$.

\subsection{Trajektoria lotu}\label{subsec:trajektoria-lotu}
\paragraph*{}
Trajektoria atmosferycznej fazy lotu ustalona jest przed startem w nastepujący sposób:
\begin{enumerate}
    \item Od startu do wysokości $1000m$ tor lotu biegnie pionowo w górę ($pitch=90^{\circ}$).
    \item Po osiągnięciu $1000m$ program ustala kierunek lotu $hdg$ na obliczony wcześniej azymut startu.
    \item Nachylenie toru lotu $pitch$ maleje liniowo od $90$ do $40$ stopni, aż do wysokości $60000m$ gdzie rozpoczyna sie egzoatmosferyczna faza lotu.
\end{enumerate}

\section{Lot egzoatmosferyczny i algorytm PEG}\label{sec:lot-egz-peg}
\paragraph*{}
Po osiągnięciu wysokości 60km, na której możemy już mówić o pomijalnym oporze aerodynamicznym, kontrolę nad lotem przejmuje algorytm PEG\@.

Pętla wywołania w trybie pasywnym trwa dopóki różnica między dwiema kolejnymi wartościami $\|\vec{v}_{go}\|$ jest większa niż $16\frac{m}{s}$.
Następnie algorytm przechodzi w tryb aktywny.

\subsection{Ustalanie komend sterowania}\label{subsec:komendy-sterowania}
\paragraph*{}
Algorytm PEG na wyjście dostarcza wersor ciągu ($\vec{\imath}_f$) w inercjalnym~(\ref{subsec:inercjalny-uklad-odniesienia}) układzie odniesienia.
Z kolei w środowisku KSP sterowanie lotem odbywa się poprzez podanie wartości kątów $pitch$ i $yaw$ względem powierzchni planety.
Po zmianie układu odniesienia dla wektora $\vec{\imath}_f$ potrzebne jest proste przetłumaczenie otrzymanego kierunku na wartości $pitch$  $yaw$:
\begin{algorytm}[Obliczanie $pitch$ i $yaw$ dla wersora ciągu $\vec{\imath}_{fs}$ określonego względem powierzchni Ziemi~\cite{KRPC}]
    \ \\
    \begin{algorithmic}
        \STATE $horizon\_direction:=\left(0, i_{fs}[1], i_{fs}[2]\right)$
        \STATE $pitch:=\arccos(i_{fs} \circ horizon\_direction)$
        \IF {$i_{fs}[0] < 0$}
        \STATE $pitch = -pitch$
        \ENDIF
        \STATE $north\_direction:=\left(0, 0, 0\right)$
        \STATE $yaw:=\arccos(i_{fs} \circ north\_direction)$
        \RETURN $pitch,yaw$
    \end{algorithmic}
\end{algorytm}

Dodatkowo jeśli aktualna faza lotu przebiega w trybie stałego przyspieszenia, konieczne jest również obliczenie stopnia otwarcia przepustnicy:

\begin{algorytm}[Ustalanie stopnia otwarcia przepustnicy]
    \ \\
    \begin{algorithmic}
        \IF {$TM_i = const\_acceleration$}
        \STATE $throttle := m\cdot a_{max,i}/f_{T,i}$
        \ELSE
        \STATE $throttle := 1$
        \ENDIF
        \RETURN $throttle$
    \end{algorithmic}
\end{algorytm}

Tak obliczone wartości $pitch, yaw$ oraz $throttle$ są przekazywane do symulacji.

\subsection{Moment zakończenia manewru}\label{subsec:moment-zakonczenia-manewru}
\paragraph*{}
Program kończy manewr, kiedy spełniony jest conajmniej jeden z warunków:
\begin{itemize}
    \item $v_{go}<16$ oraz wartość prędkości sterowanego statku odbiega od docelowej o mniej niż $16\frac{m}{s}$,
    \item wartość $t_{go}$ osiąga 0,
    \item wartość $t_{burn}$ dla ostatniej fazy lotu osiąga 0.
\end{itemize}

\section{Przykładowy lot}\label{sec:przykladowy-lot}
\paragraph*{}
Jako demonstrację działania programu pokażemy lot na orbitę kołową o następujących parametrach:
\begin{itemize}
    \item $ap=200km$,
    \item $pe=200km$,
    \item $i=20^\circ$,
    \item $\Omega=100^\circ$,
\end{itemize}
dla której wejście do algorytmu PEG ma postać:
\begin{itemize}
    \item $\gamma=0^\circ$,
    \item $\vec{\imath}_y=[-0.337, -0.06, -0.94]$,
    \item $r_d=800km$,
    \item $v_d=2101.07 \frac{m}{s}$.
\end{itemize}
\begin{figure}[h]
    \caption{Trajektoria docelowa lotu testowego.}
    \centering
    \includegraphics[width=0.7\textwidth]{./img/orb_test_flight.png}
    \label{fig:orb-test}
\end{figure}
\paragraph*{}
Następnie, po wprowadzeniu specyfikacji celu do metod obliczających parametry startu otrzymujemy następujący czas oraz azymut startu:
\begin{itemize}
    \item $t_0=4:26:24$,
    \item $hdg=68.23^\circ$.
\end{itemize}
\paragraph*{}
Misja, której specyfikacja zostanie przekazana do programu oraz algorytmu PEG będzie składała się z dwóch faz,
które odpowiadają dwóm stopniom zastosowanej rakiety nośnej.
Faza pierwsza przebiega w trybie stałego ciągu przy następujących parametrach pierwszego stopnia rakiety:
\begin{itemize}
    \item $I_{sp,1}=300\frac{m}{s}$,
    \item $f_{T,1}=1000000N$,
    \item $\dot{m_1}=342\frac{kg}{s}$,
    \item $m_1=79950kg$,
    \item $t_{burn,1}=142s$.
\end{itemize}
\paragraph*{}
Z kolei w drugiej fazie lotu program sterujący utrzymuje stałe
przyspieszenie $9\frac{m}{s^2}$ wykorzystując drugi stopień rakiety o parametrach:
\begin{itemize}
    \item $I_{sp,2}=350\frac{m}{s}$,
    \item $f_{T,2}=125000N$,
    \item $\dot{m_2}=36\frac{kg}{s}$,
    \item $m_2=18730kg$,
    \item $t_{burn,2}=328s$.
\end{itemize}
\paragraph*{}
Po 5 minutach i 57 sekundach lotu program z czego przez 142 sekundy pracował pierwszy stopień, a przez pozostałe 315 sekund stopień drugi,
program zakończył pracę.
W momencie zakończenia manewru statek znalazł się na orbicie o parametrach:
\begin{itemize}
    \item $ap=200km$,
    \item $pe=185km$,
    \item $i=20.08^\circ$,
    \item $\Omega=100.1^\circ$.
\end{itemize}
\paragraph*{}
Jak widać wysokość perycentrum nie realizuje dokładnie zadanego celu,
jednak wartość prędkości w momencie zakończenia manewru wynosi $2093\frac{m}{s}$, tj.
tylko o $7\frac{m}{s}$ mniej niż prędkość docelowa, co czyni orbitę w pełni możliwą do skorygowania
za pomocą silników manewrowych podczas najbliższego przelotu przez punkt apocentrum.
