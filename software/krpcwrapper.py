#
# Author: Bartosz Sobol
# 05.08.2019
#

import krpc
import numpy as np

from software.structs import VesselState


class KRPCWrapper:
    """
    Klasa zarzadzajaca polaczeniem ze srodowiskiem Kerbal Space Program
    """
    def __init__(self):
        self.ksp_connection = krpc.connect(name="PEG")
        self.space_center = self.ksp_connection.space_center
        self.vessel = self.ksp_connection.space_center.active_vessel

        self.kerbin_reference_frame = self.vessel.orbit.body.reference_frame
        self.orbital_reference_frame = self.vessel.orbit.body.non_rotating_reference_frame

        self.altitude_ = self.ksp_connection.add_stream(getattr, self.vessel.flight(), 'mean_altitude')
        self.time_ = self.ksp_connection.add_stream(getattr, self.ksp_connection.space_center, 'ut')
        self.mass_ = self.ksp_connection.add_stream(getattr, self.vessel, 'mass')
        self.position_ = self.ksp_connection.add_stream(self.vessel.position, self.orbital_reference_frame)
        self.velocity_ = self.ksp_connection.add_stream(self.vessel.velocity, self.orbital_reference_frame)
        self.surface_speed = self.ksp_connection.add_stream(getattr,
                                                            self.vessel.flight(self.kerbin_reference_frame), 'speed')
        self.launch_time = None

    def get_current_state(self):
        """
        :return: aktualny stan statku
        """
        v = self.velocity_()
        r = self.position_()
        v = np.array([v[0], v[2], v[1]])
        r = np.array([r[0], r[2], r[1]])
        return VesselState(self.time_() - self.launch_time, self.mass_(), r, v)


krpc_main = KRPCWrapper()
