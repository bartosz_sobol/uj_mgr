#
# Author: Bartosz Sobol
# 21.04.2019
#

import time

from software.ATG import ATG
from software.missions.missions import *
from software.utils import *
from software.PEG import PEG
from software.globals import *
from software.krpcwrapper import krpc_main as k


def main():
    mission = test_mission_2()
    launch_heading = compute_launch_heading(mission)

    if DEBUG:
        print("MISSION CONFIG")
        print("Launch hdg:", launch_heading)
        print("PEGTaregt:\n\tgamma: {}\n\ti_y: {}\n\tv_d: {}\n\tr_d: {}".format(mission.target.gamma,
                                                                                mission.target.i_y,
                                                                                mission.target.v_d_val,
                                                                                mission.target.r_d_val))
        print("Mission:\n\tno_stages: {}\n\ta_max: {}\n\tisp: {}\n\tthrust: {}\n\tmass_flow: {}\n"
              "\tmas: {}\n\tmax_burn_time: {}".format(mission.no_stages,
                                                      mission.a_max,
                                                      mission.isp,
                                                      mission.thrust,
                                                      mission.mass_flow,
                                                      mission.mass,
                                                      mission.max_burn_time))

    atg = ATG(ATG_TARGET_ALT, ATG_TARGET_PITCH, launch_heading)

    launch_time = k.time_()

    if k.vessel.situation == k.space_center.VesselSituation.pre_launch:
        print("Vessel ", k.vessel.name, " awaiting launch...")
        input("Press enter to launch")
        print('Launch!')
        time_to_launch = compute_time_to_launch(mission)
        launch_time += time_to_launch
        k.launch_time = launch_time
        if DEBUG:
            print("Launch time:", launch_time)
        k.space_center.warp_to(launch_time)
        k.vessel.control.sas = False
        k.vessel.control.rcs = False
        k.vessel.control.throttle = DEFAULT_THROTLE
        k.vessel.control.activate_next_stage()

    k.vessel.auto_pilot.engage()
    while k.altitude_() < ATG_TARGET_ALT:
        alt = k.altitude_()
        pitch, heading = atg(alt)
        if DEBUG:
            print("ATG guidance:\n\talt: {}\n\tpitch: {}\n\thdg: {}".format(alt, pitch, heading))
        k.vessel.auto_pilot.target_pitch_and_heading(pitch, heading)
        time.sleep(1)
    k.vessel.auto_pilot.disengage()

    peg = PEG(mission, k.get_current_state(), k.time_() - launch_time)

    conv_i = 0
    while conv_i < PEG_CONV_MAX_I:
        _ = peg(k.get_current_state(), PEGMode.PASSIVE)
        v_go_1 = np.linalg.norm(peg.v_go)
        _ = peg(k.get_current_state(), PEGMode.PASSIVE)
        v_go_2 = np.linalg.norm(peg.v_go)
        conv_i += 1
        if DEBUG:
            print("PEG Convergence step:  v_go_1 {}; v_go_2 {}".format(v_go_1, v_go_2))
        if np.abs(v_go_1 - v_go_2) < PEG_CONV_CRITERION:
            break
    if conv_i < PEG_CONV_MAX_I:
        print("Convergence after {} iterations".format(conv_i))
    else:
        print("PEG did not converge")
        exit(conv_i)

    k.vessel.auto_pilot.engage()
    while True:
        guidance = peg(k.get_current_state(), PEGMode.ACTIVE)
        k.vessel.auto_pilot.target_pitch_and_heading(guidance.pitch, guidance.hdg)
        k.vessel.control.throttle = guidance.throttle

        if DEBUG:
            print_peg_guidance(guidance)

        if peg.t_burn[peg.curr_stage] < 1 and peg.curr_stage < peg.stages - 1:
            k.vessel.control.throttle = 0
            time.sleep(0.2)
            k.vessel.control.activate_next_stage()
            peg.curr_stage += 1
            time.sleep(0.2)
            continue

        if np.linalg.norm(peg.v_go) < PEG_SPEED_THRESHOLD and np.abs(
                k.surface_speed() - mission.target.v_d_val) < PEG_SPEED_THRESHOLD:
            print("velocity end")
            break

        if guidance.t_go < 0:
            print('t_go end')
            break

        if np.sum(peg.t_burn) < 0:
            print("t_burn end")
            break

        time.sleep(.1)
    k.vessel.auto_pilot.disengage()
    k.vessel.control.throttle = 0

    print("End of mission")


def print_peg_guidance(guidance):
    print("PEG guidance:\n\tpitch: {}\n\thdg: {}\n\ti_f: {}\n\tt_go: {}\t\n throttle: {}".format(guidance.pitch,
                                                                                                 guidance.hdg,
                                                                                                 guidance.i_f,
                                                                                                 guidance.t_go,
                                                                                                 guidance.throttle))


if __name__ == '_main_':
    main()
main()
