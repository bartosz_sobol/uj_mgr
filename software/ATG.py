#
# Author: Bartosz Sobol
#

class ATG:
    """
    Klasa opakuwujaca metode sterowania w locie atmosferycznym (na podstawie sekcji 4.2.2)
    """
    def __init__(self, target_altitude, target_pitch, target_heading):
        self.turn_start_altitude = 1000
        self.turn_end_altitude = target_altitude
        self.target_pitch = target_pitch
        self.target_hdg = target_heading

    def __call__(self, altitude):
        if altitude < self.turn_start_altitude:
            return 90, 90
        else:
            frac = (altitude - self.turn_start_altitude) / (self.turn_end_altitude - self.turn_start_altitude)
            turn_angle_pitch = frac * self.target_pitch
            return 90 - turn_angle_pitch, self.target_hdg
