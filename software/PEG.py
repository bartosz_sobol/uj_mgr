#
# Author: Bartosz Sobol
# 21.04.2019
#

from software.CSE import CSE
from software.utils import *
from software.globals import *
from software.krpcwrapper import krpc_main as k
import numpy as np


class PEG:
    """
    Klasa opakowujaca algorytm Powered Explicit Guidance (na podstawie Rozdzialu 2)
    """
    def __init__(self, mission: Mission, init_state: VesselState, time_from_liftoff: int):
        self.call = 0
        self.kerbin_mu = k.vessel.orbit.body.gravitational_parameter
        self.mission = mission
        self.state = init_state
        self.curr_stage = 0
        self.cse = CSE()
        self.stages = mission.no_stages

        # feedback stored as class variables
        self.r_bias = np.zeros(3)
        self.r_d = unit(
            rodrigues_rotation(self.state.r, 20, -self.mission.target.i_y)) * self.mission.target.r_d_val
        self.r_grav = -self.kerbin_mu / 2 * self.state.r / np.linalg.norm(self.state.r) ** 3
        self.t_feedback = self.state.t
        self.v_feedback = self.state.v
        self.dt_feedback = 0
        self.t_go_feedback = 0
        self.v_go = self.mission.target.v_d_val * unit(np.cross(-self.mission.target.i_y, self.r_d)) - self.state.v

        # block 1 (na podstawie sekcji 26)
        self.thrust_mode = mission.thrust_mode
        self.a_max = mission.a_max
        self.a_thrust = np.divide(mission.thrust, mission.mass)
        self.v_exhaust = mission.isp * G_0
        self.mass_flow = mission.mass_flow
        self.thrust = mission.thrust
        self.tau = np.divide(self.v_exhaust, self.a_thrust)
        self.t_burn = mission.max_burn_time
        self.t_burn[self.curr_stage] -= time_from_liftoff

        if DEBUG:
            print("INIT a_t: ", self.a_thrust)
            print("INIT tau: ", self.tau)
            print("INIT thtust: ", self.thrust)
            print("INIT mass_fl: ", self.mass_flow)
            print("INIT v_exhaust: ", self.v_exhaust)

    def __call__(self, state: VesselState, mode: PEGMode):
        self.call += 1
        self.state = state
        if DEBUG:
            print(self)

        dt, dv = self.block_2()

        L_arr, t_go_arr = self.block_3()
        t_go = t_go_arr[-1]

        J_arr, S_arr, Q_arr, P_arr, H = self.block_4(L_arr, t_go_arr)

        v_bias, v_thrust, r_thrust, i_f = self.block_5(t_go,
                                                       np.sum(L_arr), np.sum(J_arr),
                                                       np.sum(S_arr), np.sum(Q_arr),
                                                       np.sum(P_arr), H)

        pitch, hdg = self.block_6(i_f)

        v_grav = self.block_7(v_thrust, r_thrust, t_go)

        self.block_8(t_go, v_grav, v_bias, r_thrust)

        self.t_go_feedback = t_go
        self.v_feedback = state.v
        self.t_feedback = state.t
        self.dt_feedback = dt
        throttle = self.block_9()
        return Guidance(pitch, hdg, np.array([i_f[0], i_f[2], i_f[1]]), t_go, throttle)

    def block_2(self):
        """
        Blok 8 (na podstawie sekcji 2.7)
        """
        dt = self.state.t - self.t_feedback
        dv = self.state.v - self.v_feedback
        self.v_go -= dv
        self.t_burn[self.curr_stage] -= self.dt_feedback
        return dt, dv

    def block_3(self):
        """
        Blok 8 (na podstawie sekcji 2.8)
        """
        if self.thrust_mode[self.curr_stage == ThrustMode.CONST_THRUST]:
            self.a_thrust[self.curr_stage] = self.thrust[self.curr_stage] / self.state.m
        else:
            self.a_thrust[self.curr_stage] = self.a_max[self.curr_stage]
        self.tau[self.curr_stage] = self.v_exhaust[self.curr_stage] / self.a_thrust[self.curr_stage]
        L_arr = list()
        t_go_arr = list()
        for s in range(self.stages - 1):
            if self.thrust_mode[s] == ThrustMode.CONST_THRUST:
                L_arr.append(self.v_exhaust[s] * np.log(self.tau[s] / (self.tau[s] - self.t_burn[s])))
            else:
                L_arr.append(self.a_max[s] * self.t_burn[s])
            if sum(L_arr) > np.linalg.norm(self.v_go):
                self.stages -= 1

        L_arr.append(np.linalg.norm(self.v_go) - sum(L_arr))

        for s in range(self.stages):
            if self.thrust_mode[s] == ThrustMode.CONST_THRUST:
                self.t_burn[s] = self.tau[s] * (1 - np.exp(-L_arr[s] / self.v_exhaust[s]))
            else:
                self.t_burn[s] = L_arr[s] / self.a_max[s]
            if s == 0:
                t_go_arr.append(self.t_burn[s])
            else:
                t_go_arr.append(t_go_arr[s - 1] + self.t_burn[s])

        return np.array(L_arr), np.array(t_go_arr)

    def block_4(self, L_arr: np.array, t_go_arr: np.array):
        """
        Blok 8 (na podstawie sekcji 2.9)
        """
        J_arr = list()
        S_arr = list()
        Q_arr = list()
        P_arr = list()
        J = S = Q = P = L = H = 0

        for s in range(self.stages):
            t_go_s1 = t_go_arr[s - 1] if s > 0 else 0
            if self.thrust_mode[s] == ThrustMode.CONST_THRUST:
                J_arr.append(self.tau[s] * L_arr[s] - self.v_exhaust[s] * self.t_burn[s])
                S_arr.append(-J_arr[s] + self.t_burn[s] * L_arr[s])
                Q_arr.append(S_arr[s] * (self.tau[s] + t_go_s1) - 0.5 * self.v_exhaust[s] * self.t_burn[s] ** 2)
                P_arr.append(Q_arr[s] * (self.tau[s] + t_go_s1) - 0.5 * self.v_exhaust[s] * self.t_burn[s] ** 2 * (
                        self.t_burn[s] / 3 + t_go_s1))
            else:
                J_arr.append(0.5 * L_arr[s] * self.t_burn[s])
                S_arr.append(J_arr[s])
                Q_arr.append(S_arr[s] * (self.t_burn[s] / 3 + t_go_s1))
                P_arr.append(S_arr[s] * (t_go_arr[s] ** 2 + 2 * t_go_arr[s] * t_go_s1 + 3 * t_go_s1 ** 2) / 6)
            J_arr[s] += L_arr[s] * t_go_s1
            S_arr[s] += L * self.t_burn[s]
            Q_arr[s] += J * self.t_burn[s]
            P_arr[s] += H * self.t_burn[s]

            L += L_arr[s]
            J += J_arr[s]
            S += S_arr[s]
            Q += Q_arr[s]
            P += P_arr[s]
            H = J * t_go_arr[s] - Q

        return np.array(J_arr), np.array(S_arr), np.array(Q_arr), np.array(P_arr), H

    def block_5(self, t_go, L, J, S, Q, P, H):
        """
        Blok 8 (na podstawie sekcji 2.10)
        """
        lbd = unit(self.v_go)
        if self.t_go_feedback > 0:
            self.r_grav *= (t_go / self.t_go_feedback) ** 2
        r_go = self.r_d - (self.state.r + self.state.v * t_go + self.r_grav)
        i_z = unit(np.cross(self.r_d, self.mission.target.i_y))
        r_go_xy = r_go - np.dot(i_z, r_go) * i_z
        r_go_z = (S - np.dot(lbd, r_go_xy)) / np.dot(lbd, i_z)
        r_go = r_go_xy + r_go_z * i_z + self.r_bias
        lbd_div = Q - S * J / L
        lbd_dot = (r_go - S * lbd) / lbd_div
        i_f = unit(lbd - lbd_dot * J / L)
        phi = np.arccos(np.dot(i_f, lbd))
        phi_dot = -phi * L / J
        v_thrust = L - 0.5 * L * phi ** 2 - J * phi * phi_dot - 0.5 * H * phi_dot ** 2
        v_thrust = v_thrust * lbd - (L * phi + J * phi_dot) * unit(lbd_dot)
        r_thrust = S - 0.5 * S * phi ** 2 - Q * phi * phi_dot - 0.5 * P * phi_dot ** 2
        r_thrust = r_thrust * lbd - (S * phi + Q * phi_dot) * unit(lbd_dot)
        v_bias = self.v_go - v_thrust
        self.r_bias = r_go - r_thrust
        if DEBUG:
            print("r_go: ", r_go)
        return v_bias, v_thrust, r_thrust, i_f

    def block_6(self, i_f):
        """
        Blok 8 (na podstawie sekcji 2.11 oraz 4.3.1)
        """
        i_ff = np.array([i_f[0], i_f[2], i_f[1]])
        orbital_frame = k.orbital_reference_frame
        surface_frame = k.vessel.surface_reference_frame
        i_ff_surface = k.space_center.transform_direction(i_ff, orbital_frame, surface_frame)
        horizon_ff = (0, i_ff_surface[1], i_ff_surface[2])

        pitch = vangle(i_ff_surface, horizon_ff)
        if i_ff_surface[0] < 0:
            pitch = -pitch

        north = [0, 1, 0]
        hdg = vangle(north, horizon_ff)
        if horizon_ff[2] < 0:
            hdg = 360 - hdg
        return pitch, hdg

    def block_7(self, v_thrust, r_thrust, t_go):
        """
        Blok 8 (na podstawie sekcji 2.12)
        """
        rc1 = self.state.r - 0.1 * r_thrust - v_thrust * t_go / 30
        vc1 = self.state.v + 1.2 * r_thrust / t_go - 0.1 * v_thrust
        rc2, vc2 = self.cse(rc1, vc1, t_go)
        v_grav = vc2 - vc1
        self.r_grav = rc2 - rc1 - vc1 * t_go
        return v_grav

    def block_8(self, t_go, v_grav, v_bias, r_thrust):
        """
        Blok 8 (na podstawie sekcji 2.13)
        """
        i_y = self.mission.target.i_y
        gamma = self.mission.target.gamma
        r_p = self.state.r + self.state.v * t_go + self.r_grav + r_thrust
        r_p -= np.dot(r_p, i_y) * i_y
        self.r_d = self.mission.target.r_d_val * unit(r_p)
        i_x = unit(self.r_d)
        i_z = np.cross(i_x, i_y)
        v_d = self.mission.target.v_d_val * np.column_stack((i_x, i_y, i_z)).dot(
            np.array([sind(gamma), 0, cosd(gamma)]))
        self.v_go = v_d - self.state.v - v_grav + v_bias / 2

    def block_9(self):
        """
        Blok 9 (na podstawie sekcji 2.14 oraz 4.3.1)
        """
        if self.thrust_mode[self.curr_stage] == ThrustMode.CONST_ACCELERATION:
            throttle = min(self.state.m * self.a_max[self.curr_stage] / (2 * self.thrust[self.curr_stage]), 1)
            if DEBUG:
                print("THROTTLE: ", throttle)
        else:
            throttle = DEFAULT_THROTLE
        return throttle

    def __repr__(self):
        r = "PEG instance, call: {}\n".format(self.call)
        r += "____________________________________________________________\n"
        r += "Flight state: \n\tt: {} \n\tv:{}, \n\tr:{}, \n\tm:{}\n".format(self.state.t,
                                                                             self.state.v,
                                                                             self.state.r,
                                                                             self.state.m)
        r += "Feedback variables\n"
        r += "\tr_bias: {}\n".format(self.r_bias)
        r += "\tr_d: {}\n".format(self.r_d)
        r += "\tr_grav: {}\n".format(self.r_grav)
        r += "\tt_feedback: {}\n".format(self.t_feedback)
        r += "\tv_feedback: {}\n".format(self.v_feedback)
        r += "\tdt_feedback: {}\n".format(self.dt_feedback)
        r += "\tt_go_feedback: {}\n".format(self.t_go_feedback)
        r += "\tv_go: {}\n".format(self.v_go)
        r += "\tt_burn: {}\n".format(self.t_burn)
        return r
