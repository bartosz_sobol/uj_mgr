#
# Author: Bartosz Sobol
# 05.08.2019
#

from collections import namedtuple
from dataclasses import dataclass
from enum import Enum
from typing import List
import numpy as np


class ThrustMode(Enum):
    """
    Typ wyliczeniowy reprezentujacy tryb pracy algorytmu PEG (na podstawie Rozdzialu 2)
    """
    CONST_THRUST = 1
    CONST_ACCELERATION = 2


class PEGMode(Enum):
    """
    Typ wyliczeniowy reprezentujacy tryb ciagu w fazie algorytmu PEG (na podstawie Rodzialu 2)
    """
    PASSIVE = 1
    ACTIVE = 2


VesselState = namedtuple('VesselState', ['t', 'm', 'r', 'v'])
Target = namedtuple('Target', ['inclination', 'lan', 'periapsis', 'apoapsis'])
PEGTarget = namedtuple('PEGTarget', ['gamma', 'i_y', 'r_d_val', 'v_d_val'])
Guidance = namedtuple('Guidance', ['pitch', 'hdg', 'i_f', 't_go', 'throttle'])  # hdg=yaw


@dataclass
class Mission:
    """
    Klasa danych skupiajaca konfiguracje misji dla algorytmu PEG
    """
    no_stages: int
    orbit_target: Target
    target: PEGTarget
    thrust_mode: List[ThrustMode]
    a_max: np.array
    isp: np.array
    thrust: np.array
    mass_flow: np.array
    mass: np.array
    max_burn_time: np.array
