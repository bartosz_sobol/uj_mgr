#
# Author: Bartosz Sobol
#

from software.utils import *
import numpy as np


def test_mission_0():
    return Mission(no_stages=2,
                   orbit_target=Target(0, 0, 200000, 200000),
                   target=preparePEGTarget(Target(0, 0, 200000, 200000)),
                   thrust_mode=[ThrustMode.CONST_THRUST, ThrustMode.CONST_THRUST],
                   a_max=np.zeros(2),
                   isp=np.array([300., 350.]),  #
                   thrust=np.array([1000000., 125000.]),  # N
                   mass_flow=np.array([342., 36.]),
                   mass=np.array([79930., 18730.]),  # kg
                   max_burn_time=np.array([142., 328.])  # s
                   )


def test_mission_1():
    return Mission(no_stages=2,
                   orbit_target=Target(20, 100, 200000, 200000),
                   target=preparePEGTarget(Target(20, 100, 200000, 200000)),
                   thrust_mode=[ThrustMode.CONST_THRUST, ThrustMode.CONST_ACCELERATION],
                   a_max=np.array([1., 9.]),
                   isp=np.array([300., 350.]),  #
                   thrust=np.array([1000000., 125000.]),  # N
                   mass_flow=np.array([342., 36.]),
                   mass=np.array([79930., 18730.]),  # kg
                   max_burn_time=np.array([142., 328.])  # s
                   )


def test_mission_2():
    return Mission(no_stages=2,
                   orbit_target=Target(20, 100, 200000, 400000),
                   target=preparePEGTarget(Target(20, 100, 200000, 400000)),
                   thrust_mode=[ThrustMode.CONST_THRUST, ThrustMode.CONST_ACCELERATION],
                   a_max=np.array([1., 8.]),
                   isp=np.array([300., 350.]),  #
                   thrust=np.array([1000000., 125000.]),  # N
                   mass_flow=np.array([342., 36.]),
                   mass=np.array([79930., 18730.]),  # kg
                   max_burn_time=np.array([142., 328.])  # s
                   )
