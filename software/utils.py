#
# Author: Bartosz Sobol
# 04.06.2019
#

from software.krpcwrapper import krpc_main as k
from software.structs import *


def preparePEGTarget(target: Target) -> PEGTarget:
    """
    Wyznaczanie parametrow celu dla algorytmu PEG (na podstawie sekcji 4.1)
    :param target: parametry celu w postaci ('inclination', 'lan', 'periapsis', 'apoapsis')
    :return: parametry celu w postaci ('gamma', 'i_y', 'r_d_val', 'v_d_val')
    """
    kerbin_mu = k.vessel.orbit.body.gravitational_parameter
    orbit_body_r = k.vessel.orbit.body.equatorial_radius
    periapsis = target.periapsis + orbit_body_r
    apoapsis = target.apoapsis + orbit_body_r
    semi_major_axis = (periapsis + apoapsis) / 2

    r_d = periapsis
    v_d = np.sqrt(kerbin_mu * (2 / r_d - 1 / semi_major_axis))

    gamma = 0

    i_sin = sind(target.inclination)
    i_cos = cosd(target.inclination)
    lan_sin = sind(target.lan)
    lan_cos = cosd(target.lan)
    rx = np.array([[1, 0, 0],
                   [0, i_cos, -i_sin],
                   [0, i_sin, i_cos]])
    rz = np.array([[lan_cos, -lan_sin, 0],
                   [lan_sin, lan_cos, 0],
                   [0, 0, 1]])
    i_y = np.matmul(rz, rx).dot([0, 0, -1])
    return PEGTarget(gamma, i_y, r_d, v_d)


def compute_launch_heading(mission: Mission):
    """
    Obliczanie azymutu startu (na podstawie sekcji 4.2.1)
    :param mission: parametry misji
    :return: azymut startu w stopniach
    """
    descending = mission.orbit_target.inclination < 0
    inclination = abs(mission.orbit_target.inclination)
    if inclination < k.vessel.flight().latitude:
        return 90
    else:
        beta_inertial = arcsind(cosd(inclination) / cosd(k.vessel.flight().latitude))
        if descending:
            if beta_inertial <= 90:
                beta_inertial = 180 - beta_inertial
            elif beta_inertial >= 270:
                beta_inertial = 540 - beta_inertial
        kerbin_velocity = k.vessel.orbit.body.rotational_speed * k.vessel.orbit.body.equatorial_radius
        v_x = mission.target.v_d_val * sind(beta_inertial) - kerbin_velocity * cosd(k.vessel.flight().latitude)
        v_y = mission.target.v_d_val * cosd(beta_inertial)
        beta_rot = arctan2d(v_x, v_y)
        return beta_rot


def compute_time_to_launch(mission: Mission):
    """
    Obliczanie czasu do startu (na podstawie sekcji 4.1.1)
    :param mission: parametry misji
    :return: pozostaly czas do startu e sekundach
    """
    slip = -1.5
    descending = mission.orbit_target.inclination < 0
    inclination = abs(mission.orbit_target.inclination)
    relative_longitude = arcsind(tand(k.vessel.flight().latitude) / tand(inclination))
    if descending:
        relative_longitude = 180 - relative_longitude

    rotational_angle = np.rad2deg(k.vessel.orbit.body.rotation_angle)
    geo_longitude = mission.orbit_target.lan + relative_longitude - rotational_angle
    geo_longitude = np.mod(geo_longitude, 360)
    node_angle = geo_longitude - k.vessel.flight().longitude
    node_angle = np.mod(node_angle + slip, 360)
    launch_time = (node_angle / 360) * k.vessel.orbit.body.rotational_period
    return launch_time


def rodrigues_rotation(vec: np.array, angle, axis: np.array):
    """
    Obrot wektora o kat wzgledem osi
    :param vec: wektor
    :param angle: kat
    :param axis: os
    :return: obrocony kat
    """
    k = unit(axis)
    sin_ = sind(angle)
    cos_ = cosd(angle)
    return vec * cos_ + np.cross(k, vec) * sin_ + (1 - cos_) * np.dot(k, vec) * k


def unit(vec: np.array):
    """
    :param vec: wektor
    :return: znormalizowany wektor vec
    """
    norm = np.linalg.norm(vec)
    return vec if norm == 0 else vec / norm


def vangle(vec_1: np.array, vec_2: np.array):
    """
    :param vec_1: wektor 1
    :param vec_2: wektor 1
    :return: kat miedzy wektorami 1 i 2 w stopniach
    """
    return arccosd(np.dot(unit(vec_1), unit(vec_2)))


def cosd(angle):
    """
   :param angle: kat w stopniach
   :return: cosinus kata angle
   """
    return np.cos(np.deg2rad(angle))


def arccosd(angle):
    """
   :param angle: cos kata
   :return: kat w stopniach
   """
    return np.rad2deg(np.arccos(angle.clip(-1.0, 1.0)))


def sind(angle):
    """
   :param angle: kat w stopniach
   :return: sinus kata angle
   """
    return np.sin(np.deg2rad(angle))


def arcsind(angle):
    """
   :param angle: sin kata
   :return: kat w stopniach
   """
    return np.rad2deg(np.arcsin(angle.clip(-1.0, 1.0)))


def tand(angle):
    """
    :param angle: kat w stopniach
    :return: tangens kata angle
    """
    return np.tan(np.deg2rad(angle))


def arctan2d(x, y):
    """
    :param x:
    :param y:
    :return: arcus tangens x/y w stopniach
    """
    return np.rad2deg(np.arctan2(x, y))
