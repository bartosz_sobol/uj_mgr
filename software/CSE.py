#
# Author: Bartosz Sobol
# 16.06.2019
#

import numpy as np
from software.krpcwrapper import krpc_main as k


class CSE:
    """
    Klasa opakowujaca metode Conic State Exprapolation (na podstawie sekcji 2.12.1)
    """
    def __init__(self, eps=1.e-8, n_max=1000):
        self.mu = k.vessel.orbit.body.gravitational_parameter
        self.eps = eps
        self.n_max = n_max

    def __call__(self, r0_vec, v0_vec, dt):
        r0 = np.linalg.norm(r0_vec)
        v0 = np.linalg.norm(v0_vec)
        vr0 = np.dot(r0_vec, v0_vec) / r0
        alpha = 2 / r0 - v0 ** 2 / self.mu
        chi = self.kepler(dt, r0, vr0, alpha)
        f, g = self.fg_coeffs(chi, dt, r0, alpha)
        r_vec = f * r0_vec + g * v0_vec
        df, dg = self.d_fg_coeffs(chi, np.linalg.norm(r_vec), r0, alpha)
        v_vec = df * r0_vec + dg * v0_vec
        return r_vec, v_vec

    def kepler(self, t, r0, vr0, alpha):
        """
        Wyznaczanie anomalii ogolnej (na podstawie Algorytmu 4 z sekcji 2.12.1)
        """
        mu_sqrt = np.sqrt(self.mu)
        chi = np.sqrt(self.mu) * np.abs(alpha) * t
        for _ in range(self.n_max):
            z = alpha * chi ** 2
            C = CSE.stumpff_c(z)
            S = CSE.stumpff_s(z)
            F = r0 * vr0 / mu_sqrt * C * chi ** 2 + (1 - alpha * r0) * S * chi ** 3 + r0 * chi - mu_sqrt * t
            dFdchi = r0 * vr0 / mu_sqrt * C * chi * (1 - z * S) + (1 - alpha * r0) * C * chi ** 2 + r0
            ratio = F / dFdchi
            chi -= ratio
            if ratio <= self.eps:
                break
        return chi

    def fg_coeffs(self, chi, t, r0, alpha):
        """
        Obliczanie mnoznikow Lagrange'a f i g (na podstawie sekcji 2.12.1)
        """
        z = alpha * chi ** 2
        f = 1 - chi ** 2 / r0 * CSE.stumpff_c(z)
        g = t - 1 / np.sqrt(self.mu) * CSE.stumpff_s(z) * chi ** 3
        return f, g

    def d_fg_coeffs(self, chi, r, r0, alpha):
        """
        Obliczanie pochodnych mnoznikow Lagrange'a f i g (na podstawie sekcji 2.12.1)
        """
        z = alpha * chi ** 2
        df = np.sqrt(self.mu) / r / r0 * (z * CSE.stumpff_s(z) - 1) * chi
        dg = 1 - chi ** 2 / r * CSE.stumpff_c(z)
        return df, dg

    @staticmethod
    def stumpff_s(z):
        """
        Obliczanie funckcji Stumpffa S (na podstawie Definicji 6 z sekcji 2.12.1)
        """
        if z > 0:
            return (np.sqrt(z) - np.sin(np.sqrt(z))) / (np.sqrt(z)) ** 3
        if z < 0:
            return (np.sinh(np.sqrt(-z)) - np.sqrt(-z)) / (np.sqrt(-z)) ** 3
        return 1 / 6

    @staticmethod
    def stumpff_c(z):
        """
        Obliczanie funckcji Stumpffa C (na podstawie Definicji 6 z sekcji 2.12.1)
        """
        if z > 0:
            return (1 - np.cos(np.sqrt(z))) / z
        if z < 0:
            return (np.cosh(np.sqrt(-z)) - 1) / (-z)
        return 0.5